package com.ajinkya.quickstart.apps;

import com.ajinkya.quickstart.apps.constants.FileExtension;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.UnsupportedFileFormatException;
import org.apache.poi.ooxml.POIXMLDocument;
import org.apache.poi.ooxml.POIXMLException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.poifs.filesystem.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

public class ValidateEmbeddedFiles {

    private static final Logger LOGGER = LogManager.getLogger(ValidateEmbeddedFiles.class);

    private Map<String, String> bannedFiles = getDefaultBannedFiles();

    public static void main(String[] args) throws IOException, URISyntaxException {
        ValidateEmbeddedFiles validateEmbeddedFiles = new ValidateEmbeddedFiles();
        URI filesDir = Objects.requireNonNull(Thread.currentThread().getContextClassLoader().getResource("files")).toURI();
        Path tempDirPath = Paths.get(filesDir);
        try (Stream<Path> files = Files.list(tempDirPath)) {
            files.forEach(file -> {
                if (!Files.isDirectory(file)) {
                    try (InputStream inputStream = Files.newInputStream(file)) {
                        validateEmbeddedFiles.validateEmbeddedFile(inputStream, file.toFile().getName());
                    } catch (IOException e) {
                        LOGGER.error(e);
                    }
                }
            });
        }
    }

    /**
     * Validates and processes an embedded file based on its extension.
     *
     * @param inputStream The input stream of the embedded file.
     * @param fileName    The name of the embedded file.
     */
    private void validateEmbeddedFile(InputStream inputStream, String fileName) {
        if (null == inputStream) {
            LOGGER.error("Invalid input stream, ignoring the embedded file validation");
            return;
        }
        if (StringUtils.isBlank(fileName)) {
            LOGGER.error("File name not provided, ignoring the embedded file validation");
            return;
        }
        String extension = FilenameUtils.getExtension(fileName);
        try {
            if (FileExtension.DOCX.equalsIgnoreCase(extension) || FileExtension.XLSX.equalsIgnoreCase(extension)) {
                getEmbeddingForXML(inputStream, fileName);
            } else if (FileExtension.DOC.equalsIgnoreCase(extension) || FileExtension.XLS.equalsIgnoreCase(extension)) {
                getEmbeddingForBIFF(inputStream, fileName);
            }
        } catch (POIXMLException | UnsupportedFileFormatException e) {
            LOGGER.error("Unsupported file <{}>, ignoring the embedded file validation.", fileName, e);
        }
    }

    /**
     * Processes an embedded file with XML-based format (docx or xlsx).
     *
     * @param inputStream The input stream of the embedded file.
     * @param fileName    The name of the embedded file.
     */
    private void getEmbeddingForXML(InputStream inputStream, String fileName) {
        String fileExtension = FilenameUtils.getExtension(fileName);
        try {
            POIXMLDocument poixmlDocument = null;
            if (FileExtension.DOCX.equalsIgnoreCase(fileExtension)) {
                poixmlDocument = new XWPFDocument(inputStream);
            } else if (FileExtension.XLSX.equalsIgnoreCase(fileExtension)) {
                poixmlDocument = new XSSFWorkbook(inputStream);
            }
            if (null != poixmlDocument) {
                List<PackagePart> allEmbeddedParts = poixmlDocument.getAllEmbeddedParts();
                processEmbeddedParts(allEmbeddedParts, fileName);
            }
        } catch (IOException e) {
            LOGGER.error("Unable to read file <{}>, ignoring the embedded file validation.", fileName, e);
        } catch (OpenXML4JException e) {
            LOGGER.error("Unable to get embedded parts of file <{}>, ignoring the embedded file validation.", fileName, e);
        }
    }

    /**
     * Processes an embedded file with Binary Interchange file format (doc or xls).
     *
     * @param inputStream The input stream of the embedded file.
     * @param fileName    The name of the embedded file.
     */
    private void getEmbeddingForBIFF(InputStream inputStream, String fileName) {
        try (POIFSFileSystem fs = new POIFSFileSystem(inputStream)) {
            DirectoryNode root = fs.getRoot();
            for (Entry entry : root) {
                if (entry.isDirectoryEntry()) {
                    DirectoryNode directoryNode = (DirectoryNode) entry;
                    processDirectoryEntry(entry, directoryNode, fileName);
                }
            }
        } catch (IOException e) {
            LOGGER.error("Unable to read file <{}>, ignoring the embedded file validation.", fileName, e);
        }
    }

    /**
     * Processes the embedded parts of a document.
     *
     * @param allEmbeddedParts A list of PackagePart objects representing all embedded parts.
     * @param fileName         The name of the embedded file.
     */
    private void processEmbeddedParts(List<PackagePart> allEmbeddedParts, String fileName) {
        for (PackagePart packagePart : allEmbeddedParts) {
            try (POIFSFileSystem poifsFileSystem = new POIFSFileSystem(packagePart.getInputStream())) {
                processOle10Native(poifsFileSystem.getRoot(), fileName);
            } catch (IOException e) {
                LOGGER.error("Unable to read package part, ignoring the validation of package part <{}>", packagePart.getPartName(), e);
            }
        }
    }

    /**
     * Processes a directory entry within a DirectoryNode.
     *
     * @param entry         The entry to process.
     * @param directoryNode The parent DirectoryNode containing the entry.
     * @param fileName      The name of the file being processed.
     * @throws IOException If an I/O error occurs while processing the entry.
     */
    private void processDirectoryEntry(Entry entry, DirectoryNode directoryNode, String fileName) throws IOException {
        String fileExtension = FilenameUtils.getExtension(fileName);
        if (FileExtension.DOC.equalsIgnoreCase(fileExtension) && "ObjectPool".equals(entry.getName())) {
            for (Entry internalEntry : directoryNode) {
                if (internalEntry.isDirectoryEntry()) {
                    processOle10Native((DirectoryNode) internalEntry, fileName);
                }
            }
        } else if (FileExtension.XLS.equalsIgnoreCase(fileExtension)) {
            processOle10Native(directoryNode, fileName);
        }
    }

    /**
     * This method validates the embedded OLE Object/file.
     *
     * @param directoryNode directory entry
     * @param fileName      to be checked for file embeddings
     * @throws IOException if not able to extract the embedded OLE object
     */
    public <T extends DirectoryNode> void processOle10Native(T directoryNode, String fileName) throws IOException {
        try {
            Ole10Native ole10Native = Ole10Native.createFromEmbeddedOleObject(directoryNode);
            String oleFileName = ole10Native.getLabel();
            int BUFFER_SIZE = bannedFiles.values().parallelStream().reduce("", (s1, s2) -> s1.length() > s2.length() ? s1 : s2).split(" ").length;
            PushbackInputStream inputStream = new PushbackInputStream(new ByteArrayInputStream(ole10Native.getDataBuffer()), BUFFER_SIZE);
            for (Map.Entry<String, String> entry : bannedFiles.entrySet()) {
                if (StringUtils.isBlank(entry.getValue())) {
                    continue;
                }
                int readBytesLength = entry.getValue().split(" ").length;
                byte[] bytes = inputStream.readNBytes(readBytesLength);
                inputStream.unread(bytes);
                if (bytes.length == readBytesLength) {
                    String hexString = bytesToHexString(bytes);
                    if (hexString.toLowerCase().startsWith(entry.getValue().toLowerCase())) {
                        LOGGER.error("File <{}> contains banned file <{}>", fileName, oleFileName);
                    }
                } else {
                    LOGGER.warn("file is too short to validate");
                }
            }
        } catch (Ole10NativeException e) {
            LOGGER.error("Unable to extract the embedded OLE object, ignoring the validation of directory node <{}> for file <{}>", directoryNode.getName(), fileName, e);
        }
    }

    public Map<String, String> getDefaultBannedFiles() {
        Map<String, String> bannedFiles = new HashMap<>();
        bannedFiles.put("exe", "0x4D 0x5A");
        bannedFiles.put("msi", "0x4D 0x5A");
        return bannedFiles;
    }

    /**
     * converts the bytes to hex string
     *
     * @param bytes to be converted to hex string
     * @return hex string
     */
    private String bytesToHexString(byte[] bytes) {
        StringBuilder hexFormattedString = new StringBuilder();
        for (byte b : bytes) {
            hexFormattedString.append(String.format("0x%02X ", b));
        }
        return hexFormattedString.toString();
    }

    public void setBannedFiles(Map<String, String> bannedFiles) {
        this.bannedFiles = bannedFiles;
    }
}