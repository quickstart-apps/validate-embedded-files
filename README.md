**ValidateEmbeddedFiles**

This package provides a Java utility class for scanning uploaded documents for embedded OLE objects and verifying if they contain banned file signatures. It helps prevent potential security vulnerabilities arising from malicious file inclusions.

**Key Features:**

* Scans embedded files within supported document formats (docx, xlsx, doc, xls).
* Verifies extracted OLE objects against a user-defined list of banned file signatures.
* Customizable banned files list for specific needs.
* Logging and error handling for detected banned file inclusions and potential errors.

**Usage:**

1. **Download the package:** Clone the repository or download the compiled `.jar` file.
2. **Configure banned files:** Define a `Map` with banned file signatures as keys and their identifying byte sequences as values.
3. **Call the** `ValidateEmbeddedFiles` **class:**
    * Pass the directory containing uploaded documents as a Path object.
    * Optionally, provide your custom map of banned files.
4. **Analyze results:** Check logs for any reported banned file inclusions within processed documents.

**Example Usage:**

```Java
Path documentsDir = Paths.get("/path/to/uploaded/documents");
Map<String, String> bannedFiles = new HashMap<>();
bannedFiles.put("exe", "0x4D 0x5A"); // Signature for executable files

ValidateEmbeddedFiles validator = new ValidateEmbeddedFiles();
validator.setBannedFiles(bannedFiles);
validator.validateEmbeddedFiles(documentsDir);

// Check logs for any reported banned file inclusions
```

**Dependencies:**

* Apache Commons IO
* Apache Commons Lang
* Apache POI

**Contributing:**

Fork the repository and raise pull requests for improvements or new features.

**Author:**

[Ajinkya Mundankar](https://gitlab.com/ajinkya_mundankar)

**Conclusion:**

`ValidateEmbeddedFiles` helps ensure secure document processing by verifying embedded content for potentially harmful file inclusions.