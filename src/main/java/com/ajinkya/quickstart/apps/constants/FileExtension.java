package com.ajinkya.quickstart.apps.constants;

public class FileExtension {
    public static final String DOC = "doc";
    public static final String DOCX = "docx";
    public static final String XLS = "xls";
    public static final String XLSX = "xlsx";
    private FileExtension() {}
}